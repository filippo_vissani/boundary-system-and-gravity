﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public GameObject root;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        root.transform.Rotate(0, (float)(Time.deltaTime * 90), 0, Space.World);
        root.transform.Translate(Vector3.forward * (float)(Time.deltaTime * 0.5));
    }
}
