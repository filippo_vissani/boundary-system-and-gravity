Progetto utilizzato per testare la forza di gravità applicata agli ologrammi e per le collisioni degli ologrammi fra loro o con la mesh.
Nel progetto sono presenti:
- Un Rover al quale non viene applicata la forza di gravità, sono applicati i componenti RigidBody e Spatial Mapping Collider.
- Un cubo disposto su un piano, al quale viene applicata la forza di gravità, una volta spostato dal piano cadrà sulla mesh.
Il Rover è ancorato in modo persistente alla mesh, il cubo no.
Sono stati abilitati i profili Spatial Awareness (percezione ambientale) e Boundary (per rilevare collisioni con la mesh).
I componenti RigidBody e AnchorManager applicati al Rover non sono compatilibili, RigidBody disabilita le ancore.
